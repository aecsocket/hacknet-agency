import Command from './Command.js';

export default class ConnectCommand extends Command {
    get name() {
        return 'connect';
    }

    get usage() {
        return '<username>';
    }

    execute(args) {
        if (args.length < 1) {
            this.displayUsage();
            return Promise.resolve();
        }

        if (typeof args[0] !== 'string') {
            throw 'Invalid username: ' + args[0];
        }

        this.client.username = args[0];
        return this.client.connectToServer(3500, '127.0.0.1');
    }
}
