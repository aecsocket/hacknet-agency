import Command from './Command.js';

export default class ReadyCommand extends Command {
    get name() {
        return 'ready';
    }

    get usage() {
        return '';
    }

    execute(args) {
        this.client.socket.write(JSON.stringify({type: 'lobby-ready'}));
        return Promise.resolve();
    }
}
