import Command from './Command.js';

export default class ExitCommand extends Command {
    get name() {
        return 'exit';
    }

    get usage() {
        return '';
    }

    execute(args) {
        process.exit(0);
        return Promise.resolve();
    }
}
