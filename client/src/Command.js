/** @typedef {import('./Client').default} Client */

export default class Command {
    /** @type {Client} */
    client;

    constructor(client) {
        this.client = client;
    }

    /**
     * @returns {string}
     */
    get name() {
        throw 'Unimplemented';
    }

    /**
     * @returns {string}
     */
    get usage() {
        throw 'Unimplemented';
    }

    /**
     * @param {string[]} args
     * @returns {Promise}
     */
    execute(args) {
        throw 'Unimplemented';
    }

    displayUsage() {
        console.log('Usage: ' + this.name + ' ' + this.usage);
    }

    log(message, isError) {
        this.client.log(message, isError);
    }
}
