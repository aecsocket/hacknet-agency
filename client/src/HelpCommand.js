import Command from './Command.js';

export default class HelpCommand extends Command {
    get name() {
        return 'help';
    }

    get usage() {
        return '';
    }

    execute(args) {
        console.log('Available commands:');
        for (const commandName in this.client.commands) {
            const command = this.client.commands[commandName];
            console.log(command.name + ' ' + command.usage);
        }
        return Promise.resolve();
    }
}
