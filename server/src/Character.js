/** @typedef {import('./World.js').default} World */
/** @typedef {import('./node/Node.js').default} Node */

/** @typedef {import('./Client.js').default} Client */
/** @typedef {import('./node/component/fs/Directory.js').default} Directory */

import log, {LogType} from './Logger.js';

import PacketTerminalMessage from './network/packet/game/PacketTerminalMessage.js';

export default class Character {
    /** @type {*} */
    dependencies = {
        homeNode: null
    }

    //=====| CHARACTER INFO |=====\\
    /** @type {string} */
    name;

    /** @type {Node} */
    homeNode;

    //=====| TECHNICAL DATA |=====\\
    /** @type {World} */
    world;
    
    id = -1;
    /** @type {Client} */
    pilot = null;

    //=====| GAME DATA |=====\\
    /** @type {*} */
    currentNode = null;

    /** @type {Directory} */
    currentDirectory = null;
    

    /**
     * @param {World} world
     * @param {number} id
     */
    constructor(world, id) {
        this.world = world;
        this.id = id;
    }

    /**
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) {
        this.homeNode = world.getNodeFromId(this.dependencies.homeNode);
        if (!this.homeNode) {
            log(LogType.ERROR, `Failed to resolve home node ${this.dependencies.homeNode} for character ${this.name}`);
            return false;
        }

        return true;
    }

    /**
     * @returns {World}
     */
    getWorld() {
        return this.world;
    }

    /**
     * Called when a pilot Client is linked to the Character
     * @param {Client} pilot
     */
    spawn(pilot) {
        this.pilot = pilot;
    }

    despawn() {
        this.pilot = null;
    }

    /**
     * @param {string} message
     */
    writeToTerminal(message, end = '\n') {
        const pack = new PacketTerminalMessage(message + end);
        if (this.pilot) {
            this.pilot.send(pack);
        }
    }

    getPilot() {
        return this.pilot;
    }

    getCharacterInfo() {
        return {id: this.id, name: this.name};
    }

    disconnect() {
        this.currentNode = null;
        this.currentDirectory = null;
    }

    /**
     * @param {Node} target
     */
    connect(target) {
        /** Send #packet @packet to notify connection */
        this.currentNode = target;
        this.currentDirectory = target.root;
    }

    changeDir(targetComp, targetDir) {
        // check if dir exists
        this.currentDirectory = targetDir;
    }
}
