/** @typedef {import('../../World.js').default} World */
/** @typedef {import('../../Character.js').default} Character */
/** @typedef {import('../../node/Node.js').default} Node */
/** @typedef {import('../../node/component/fs/Directory.js').default} Directory */
import GoalNode from './GoalNode.js';
import log, {LogType} from '../../Logger.js';

export default class GoalUpload extends GoalNode {
    type = 'upload';

    dependencies = {
        target: '',
        targetDirectory: '',
        source: '',
        sourceFile: ''
    }

    /** @type {Node} */
    source = null;
    
    sourceContents = '';

    /**
     * @param {Directory} dir
     * @returns {boolean}
     */
    checkDir(dir) {
        for (const item of dir.children) {
            if (item.type === 'dir') {
                this.checkDir(/** @type {Directory} */ (item));
            }
            else if (item.content === this.sourceContents) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param {World} world
     * @param {Character} character
     * @returns {Boolean}
     */
    check(world, character) {
        return this.checkDir(/** @type {Directory} */(this.target.getRoot().getFilesFromPath(this.dependencies.targetDirectory)));
    }

    /**
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) {
        if (!super.resolve(world)) {
            return false;
        }
        // Resolve source node
        this.source = world.getNodeFromId(this.dependencies.source);
        if (!this.source) {
            log(LogType.Error, `Failed to resolve target ${this.dependencies.source}`);
            return false;
        }
        // Resolve source file
        const sourceFile = this.source.getRoot().getFilesFromPath(this.dependencies.sourceFile);
        if (!sourceFile) {
            return false;
        }
        // Resolve source file contents
        this.sourceContents = sourceFile.content;

        return true;
    }

    /**
     * @virtual
     * @param {any} object
     * @returns {GoalUpload}
     */
    static deserialize(object) {
        const r = new GoalUpload();
        r.dependencies.source = object.source;
        r.dependencies.target = object.target;
        r.dependencies.sourceFile = object.sourceFile;
        r.dependencies.targetDirectory = object.destinationDirectory;
        return r;
    }
}
