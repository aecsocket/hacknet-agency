/** @typedef {import('../../World.js').default} World */
/** @typedef {import('../../Character.js').default} Character */

export default class Goal {
    /** @type {string} */
    type;

    constructor() {

    }

    /**
     * @virtual
     *
     * @param {World} world
     * @param {Character} character
     * @returns {boolean}
     */
    check(world, character) {
        return true;
    }

    /**
     * @virtual
     *
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) { return true; }

    /**
     * @virtual
     * @param {any} object
     * @returns {Goal}
     */
    static deserialize(object) {
        return new Goal();
    }
}
