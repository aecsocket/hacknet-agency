/** @typedef {import('../../World.js').default} World */
/** @typedef {import('../../Character').default} Character */
/** @typedef {import('../../node/Node.js').default} Node */
/** @typedef {import('../../node/component/fs/Directory.js').default} Directory */
import GoalNode from './GoalNode.js';

export default class GoalDownload extends GoalNode {
    type = 'delete';

    dependencies = {
        target: '',
        file: ''
    }

    fileContents = '';

    /**
     * @param {Directory} dir
     * @returns {boolean}
     */
    checkDir(dir) {
        for (const item of dir.children) {
            if (item.type === 'dir') {
                this.checkDir(/** @type {Directory} */ (item));
            }
            else if (item.content === this.fileContents) {
                return true;
            }
        }
        return false;
    }

    /**
     * Resolve id dependencies to object references
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) {
        // Check if node exists
        if (!super.resolve(world)) {
            return false;
        }
        // Check if file exists
        const file = this.target.getRoot().getFile(this.dependencies.file);
        if (!file) {
            return false;
        }
        // Assign file contents to a fileContents
        this.fileContents = file.content;
        return true;
    }

    /**
     * @param {World} world
     * @param {Character} character
     * @returns {boolean}
     */
    check(world, character) {
        return this.checkDir(character.homeNode.getRoot().root);
    }

    /**
     * @virtual
     * @param {any} object
     * @returns {GoalDownload}
     */
    static deserialize(object) {
        const r = new GoalDownload();
        r.dependencies.target = object.target;
        r.dependencies.file = object.file;
        return r;
    }
}
