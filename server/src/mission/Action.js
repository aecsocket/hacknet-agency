/** @typedef {import('./goal/Goal.js').default} Goal */
/** @typedef {import('../World.js').default} World */

export default class Action {
    /** @type {string} */
    type;

    /**
     * @virtual
     *
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) { return true; }
}
