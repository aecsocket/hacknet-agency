import Action from './Action.js';
/** @typedef {import('../../World.js').default} World */

export default class ActionMail extends Action {
    /** @type {string} */
    type = 'mail';

    /** @type {*} */
    msg = {
        sender: '',
        subject: '',
        body: '',
        attachments: []
    }

    constructor() {
        super();
    }

    /**
     * @virtual
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) { return true; }

    /**
     * @virtual
     * @param {any} object
     * @returns {ActionMail}
     */
    static deserialize(object) {
        const r = new ActionMail();
        r.msg = object.msg;
        if (Array.isArray(r.msg.body)) {
            r.msg.body = r.msg.body.join('\n');
        }
        return r;
    }
}
