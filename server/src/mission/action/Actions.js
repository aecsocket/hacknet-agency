import ActionEcho from './ActionEcho.js';
import ActionMail from './ActionMail.js';

const ActionTypes = {
    'echo': ActionEcho,
    'mail': ActionMail
};

export default ActionTypes;
