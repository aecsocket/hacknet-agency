import Action from './Action.js';
/** @typedef {import('../../World.js').default} World */

export default class ActionEcho extends Action {
    /** @type {string} */
    type = 'echo';

    /** @type {string} */
    msg;

    constructor() {
        super();
    }

    /**
     * @virtual
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) { return true; }

    /**
     * @virtual
     * @param {any} object
     * @returns {ActionEcho}
     */
    static deserialize(object) {
        const r = new ActionEcho();
        r.msg = object.msg;
        return r;
    }
}
