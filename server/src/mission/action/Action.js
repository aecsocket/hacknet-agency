/** @typedef {import('../../World.js').default} World */

export default class Action {
    /** @type {string} */
    type;

    /**
     * @virtual
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) { return true; }

    /**
     * @virtual
     * @param {any} object
     * @returns {Action}
     */
    static deserialize(object) {
        return new Action();
    }
}
