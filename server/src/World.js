/** @typedef {import('./Character.js').default} Character */
/** @typedef {import('./node/Node.js').default} Node */
/** @typedef {import('./mission/Mission.js').default} Mission */

import log, {LogType} from './Logger.js';
import PacketWorldInfo, { WorldInfo } from './network/packet/general/PacketWorldInfo.js';

export default class World {
    info = {
        path: '',
        name: '',
        characterAmount: {min: 1, max: 8}
    };

    /** @type {Node[]} */
    nodes = [];

    /** @type {Mission[]} */
    missions = [];

    /** @type {Character} */
    defaultCharacter = null;

    /** @type {Character[]} */
    characters = [];

    constructor() {
        this.world = undefined;
    }

    /**
     * @returns {boolean}
     */
    resolve() {
        for (const node of this.nodes) {
            if (!node.resolve(this)) {
                log(LogType.ERROR, `Resolving node ${node.id} failed.`);
                return false;
            }
        }

        for (const mission of this.missions) {
            if (!mission.resolve(this)) {
                log(LogType.ERROR, `Resolving mission ${mission.id} failed.`);
                return false;
            }
        }

        for (const character of this.characters) {
            if (!character.resolve(this)) {
                log(LogType.ERROR, `Resolving character ${character.name} failed.`);
                return false;
            }
        }

        return true;
    }

    /**
     * @returns {WorldInfo}
     */
    getDisplayData() {
        const r = [];
        for (const character of this.characters) {
            r.push(character.getCharacterInfo());
        }
        return new WorldInfo(this.info.name, r);
    }

    /**
     * @returns {PacketWorldInfo}
     */
    getWorldInfoPacket() {
        const packet = new PacketWorldInfo(this.getDisplayData());
        return packet;
    }

    getCharacters() {
        return this.characters;
    }

    /**
     * @param {number} id
     */
    getCharacterFromId(id) {
        return this.characters.find((char) => char.id === id);
    }

    /**
     * @param {string} id
     * @param {any[]} arr
     * @returns {*}
     */
    getFromId(id, arr) { return arr.find(obj => obj.id === id); }

    /**
     * @param {string} id
     * @returns {Node}
     */
    getNodeFromId(id) { return this.getFromId(id, this.nodes); }

    /**
     * @param {string} id
     * @returns {Mission}
     */
    getMissionFromId(id) { return this.getFromId(id, this.missions); }

    /**
     * @param {string} address
     */
    getNodeFromAddress(address) {
        return this.nodes.find(node => node.address === address);
    }
}
