import util from 'util';

export const LogType = {
    DEBUG: 0,
    VERBOSE: 1,
    LOG: 2,
    WARNING: 3,
    ERROR: 4,
    CRITICAL: 5
};

const logSeverity = ['DEBUG', 'VERBOSE', 'LOG', 'WARNING', 'ERROR', 'CRITICIAL'];

let logLevel = 0;

/**
 * @param {number} level
 */
export function setLogLevel(level) {
    logLevel = level;
}

/**
 * @param {number} severity
 * @param {any} msg
 * @param {any} [inspect]
 */
export default function (severity, msg, inspect) {
    if (severity < logLevel) {
        return;
    }
    process.stdout.write('[' + logSeverity[severity] + '] ');
    if (inspect) {
        console.log(util.inspect(msg, false, null, true));
    }
    else {
        console.log(msg);
    }
}
