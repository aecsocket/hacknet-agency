/** @typedef {import('../World.js').default} World */
/** @typedef {import('../Character.js').default} Character */
/** @typedef {import('./component/fs/Directory.js').default} Directory */

import log, {LogType} from '../Logger.js';

/**
 * @returns {string}
 */
export function generateAddress() {
    const forbidden = [0, 10, 100];
    let leading = 0;
    while (forbidden.indexOf(leading) > -1) {
        leading = Math.floor(Math.random() * 127);
    }
    return `${leading}.${Math.floor(Math.random() * 256)}.${Math.floor(Math.random() * 256)}.${Math.floor(Math.random() * 256)}`;
}

export default class Node {
    /** @type {*} */
    dependencies = {
        links: []
    }

    /** @type {string} */
    id;

    /** @type {string} */
    address;

    /** @type {string} */
    displayName;

    /** @type {any[]} */
    ports = [];

    /** @type {number} */
    proxy;

    /** @type {*} */
    firewall = {
        solution: '',
        additionalTime: 0,
        level: 0
    }

    /** @type {Node[]} */
    links = [];

    /** @type {*} */
    monitor = {
        type: '',
        traceTime: null
    }

    /** @type {any[]} */
    accounts = [];

    /** @type {Directory} */
    root;

    /** @type {World} */
    world;

    /** @type {World} */
    constructor(world) {
        this.world = world;
    }

    /**
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) {
        if (!this.address) {
            this.address = generateAddress();
        }

        for (let i = 0; i < this.dependencies.links.length; i++) {
            this.links[i] = world.getNodeFromId(this.dependencies.links[i]);
            if (!this.links[i]) {
                log(LogType.ERROR, `Failed to resolve link ${this.dependencies.links[i]} for node ${this.id}`);
                return false;
            }
        }
        return true;
    }

    getRoot() {
        return this.root;
    }

    /**
     * @param {Character} character
     */
    onConnect(character) {
        character.currentNode = this;
        return true;
    }

    /**
     * @param {Character} character
     */
    onDisconnect(character) {
        character.currentNode = null;
        return false;
    }
}
