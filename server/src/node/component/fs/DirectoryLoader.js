import File from './File.js';
import Directory, {Tokens} from './Directory.js';

/**
 * @param {Directory} parent
 * @param {*} data
 * @returns {File[]}
 */
function walk(parent, data) {
    const result = [];

    for (const name in data) {
        const object = data[name];
        let file = null;
        if (isDirectory(object)) {
            file = new Directory(parent);
            file.children = walk(file, object);
        }
        else {
            file = new File(parent);
            if (typeof object === 'string') {
                // Raw data file
                file.content = object;
            }
            if (Array.isArray(object)) {
                // Multi-line file
                file.content = object.join('\n');
            }
            // TODO: Handle custom file types!
        }

        file.name = name;
        result.push(file);
    }

    return result;
}

function isDirectory(data) { return typeof data === 'object' && data.constructor.name !== 'Array' && !(Tokens.COMPLEX_FILE in data); }

/**
 * @param {any} data
 */
export default function (data) {
    const result = new Directory(null);

    result.name = '';
    result.children = walk(result, data);

    return result;
}
