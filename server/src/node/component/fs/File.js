/** @typedef {import('./Directory.js').default} Directory */

export default class File {
    /** @type {string} */
    type = 'file';

    /** @type {string} */
    name = '';

    /** @type {string} */
    content = '';

    /** @type {Directory} */
    parent = null;

    /**
     * @param {?Directory} parent
     */
    constructor(parent) {
        this.parent = parent;
    }

    /**
     * @returns {Directory[]}
    */
    getParents() {
        if (this.parent === null) {
            return [];
        }
        return [this.parent].concat(this.parent.getParents());
    }

    /**
     * @returns {string}
     */
    getAbsolutePath() {
        return this.getParents().map(file => file.name).join('/') + this.name;
    }
}
