import File from './File.js';

export const Tokens = {
    DIRECTORY: '/',
    THIS: '.',
    UP: '..',
    COMPLEX_FILE: '#type',
    STAR_WILDCARD: '*',
    QUESTION_WILDCARD: '?'
};

/**
 * @param {string} pattern
 */
function isGlobPattern(pattern) {
    if (pattern.indexOf(Tokens.STAR_WILDCARD) > -1) {
        return true;
    }
    if (pattern.indexOf(Tokens.QUESTION_WILDCARD) > -1) {
        return true;
    }
    return false;
}

/**
 * @param {File} cur
 * @param {string[]} paths
 * @param {boolean} [glob]
 * @returns {File[]}
 */
function walkFind(cur, paths, glob = false) {
    if (cur === null) {
        return [];
    }
    if (paths.length === 0) {
        return [cur];
    }
    else if (cur.type !== 'dir') {
        return [];
    }
    const curDir = /** @type {Directory} */ (cur);
    const direction = paths.shift();
    switch (direction) {
    case Tokens.THIS:
        return this.walkFind(curDir, paths, glob);
    case Tokens.UP:
        return this.walkFind(curDir.getSafeParent(), paths, glob);
    default:
        if (!glob) {
            if (isGlobPattern(direction)) {
                return [];
            }
            return this.walkFind(curDir.getFile(direction), paths, glob);
        }
        {
            const files = curDir.children;
            // Do globbing
            return [];
        }
    }
}

export default class Directory extends File {
    /** @type {string} */
    type = 'dir';

    /** @type {File[]} */
    children = [];

    /**
     * @param {?Directory} parent
     */
    constructor(parent) {
        super(parent);
    }

    /**
     * @returns {Directory}
     */
    get root() {
        return this.parent ? this.parent.root : this.parent;
    }

    /**
     * @param {string} pathStr
     * @param {Directory} working
     * @returns {File[]}
     */
    getFiles(pathStr, working = this.root) {
        if (pathStr[0] === Tokens.DIRECTORY) {
            working = this.root;
            pathStr = pathStr.slice(1);
        }
        const paths = pathStr.split(Tokens.DIRECTORY);
        
        return walkFind(working, paths, true);
    }

    /**
     * @param {string} pathStr
     * @param {Directory} working
     * @returns {File}
     */
    getFile(pathStr, working = this.root) {
        if (pathStr[0] === Tokens.DIRECTORY) {
            working = this.root;
            pathStr = pathStr.slice(1);
        }
        const paths = pathStr.split(Tokens.DIRECTORY);

        const walk = walkFind(working, paths, false);
        return walk.length > 0 ? walk[0] : null;
    }


    getFileRaw(name) { return this.children.find(file => file.name === name); }
    
    /**
     * @returns {!Directory}
     */
    getSafeParent() { return !this.parent ? this : this.parent; }

    /**
     * @param {string} name
     * @param {string} [content]
     * @returns {File}
     */
    createFile(name, content = '') {
        const file = new File(this);
        file.name = name;
        file.content = content;
        return file;
    }

    /**
     * @param {File} file
     * @returns {boolean}
     */
    removeFile(file) {
        this.children = this.children.filter((f) => f !== file);
        return true;
    }
}
