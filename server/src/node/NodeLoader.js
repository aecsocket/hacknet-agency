import Node from './Node.js';
/** @typedef {import('../World.js').default} World */
import log, {LogType} from '../Logger.js';
import {Tokens} from './component/fs/Directory.js';
import DirectoryLoader from './component/fs/DirectoryLoader.js';

/**
 * @param {*} [properties]
 * @returns {string}
 */
export function generatePassword(properties = {length: 16}) {
    if (!properties.length) {
        properties.length = 16;
    }
    // @ts-ignore
    const allowed = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!?#@_-'.shuffle();
    let r = '';
    for (let i = 0; i < properties.length; i++) {
        r += allowed[Math.floor(Math.random() * allowed.length + 1)];
    }
    return r;
}

/**
 * @param {World} world
 * @param {any} data
 * @returns {Node}
 */
export default function (world, data) {
    const result = new Node(world);
    result.id = data.id;
    result.displayName = data.display_name;

    if (data.ports !== undefined && data.ports !== undefined) {
        for (const port of data.ports) {
            result.ports.push({
                num: port.num,
                protocol: port.protocol,
                open: false
            });
        }
    }

    result.proxy = data.proxy;
    result.address = data.address;
    result.firewall = data.firewall;
    result.monitor = data.monitor;

    if (Array.isArray(data.accounts)) {
        for (const accountData of data.accounts) {
            const account = {
                name: accountData.name,
                password: null,
                admin: 'admin' in accountData ? accountData.admin : false,
                home: Tokens.DIRECTORY + 'home'
            };
            if (account.admin) {
                account.home = Tokens.DIRECTORY;
            }
            if ('password' in accountData && accountData.password !== null) {
                if (typeof accountData.password === 'string') {
                    account.password = accountData.password;
                }
                else {
                    account.password = generatePassword(accountData.password);
                }
            }
            else {
                account.password = null;
            }
            result.accounts.push(account);
        }
    }

    result.root = DirectoryLoader(data.filesystem);
    result.dependencies.links = data.links ? data.links : [];

    log(LogType.VERBOSE, `Loaded node ${result.id}`);

    return result;
}
