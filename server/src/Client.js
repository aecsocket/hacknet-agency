/** @typedef {import('net').Socket} Socket */

/** @typedef {import('./Server.js').default} Server */
/** @typedef {import("./network/packet/Packet.js").default} Packet */

/** @typedef {import('./automaton/client/State.js').default} State */

/** @typedef {import('./Character.js').default} Character */

import PacketKeepAlive from './network/packet/system/PacketKeepAlive.js';
import PacketDisconnected from './network/packet/system/PacketDisconnected.js';
import PacketCharacterLink from './network/packet/general/PacketCharacterLink.js';
import {ClientInfo} from './network/packet/general/PacketClientsInfo.js';

import ConnectingState from './automaton/client/ConnectingState.js';

import log, {LogType} from './Logger.js';

const TIMEOUT_TIME = 10;

export const ClientStatus = {
    DISABLED: 0,
    CONNECTING: 1,
    CONNECTED: 2,
    INGAME: 3
};

export default class Client {
    /** @type {Server} */
    server;

    /** @type {Socket} */
    socket;

    /** @type {number} */
    id = -1;

    /**
     * {@code true} when waiting for client response, {@code false} when connection is acknowledged
     */
    connectionHeartBeat = false;

    status = ClientStatus.CONNECTING;

    /** @type {State} */
    state;

    /** @type {string} */
    username;

    /** @type {Character} */
    character;

    /**
     * @param {Server} server
     * @param {Socket} socket
     * @param {number} id
     */
    constructor(server, socket, id) {
        this.server = server;
        this.socket = socket;
        this.id = id;
    }

    init() {
        this.socket.setNoDelay();
        this.socket.on('data', (data) => this.onDataReceived(data));
        this.socket.on('end', () => this.disconnect('Connection closed'));
        this.keepAlive();
        this.setState(new ConnectingState(this));
    }

    keepAlive() {
        if (this.connectionHeartBeat) {
            this.disconnect('Connection timeout');
            return;
        }
        const ka = new PacketKeepAlive();
        this.send(ka);
        this.connectionHeartBeat = true;
        setTimeout(() => this.keepAlive(), TIMEOUT_TIME * 1000);
    }

    /**
     * @param {string} [reason]
     */
    disconnect(reason) {
        if (this.status === ClientStatus.DISABLED) {
            return;
        }
        this.status = ClientStatus.DISABLED;
        if (!this.socket.destroyed) {
            if (reason) {
                const packet = new PacketDisconnected(reason);
                this.send(packet);
            }
            this.socket.end();
        }
        this.unlinkCharacter();

        // @ts-ignore
        log(LogType.LOG, `Client ${this.username} at ${this.socket.address().address} disconnected: ${reason}.`);

        this.server.disconnect(this);
    }

    /**
     * @param {!State} state
     */
    setState(state) {
        if (this.state) {
            this.state.exit();
        }
        this.state = state;
        this.state.enter();
    }


    /**
     * @param {any} data
     */
    onDataReceived(data) {
        let packet;
        try {
            //console.log(data.toString());
            packet = JSON.parse(data.toString());
        }
        catch (exception) {
            return;
        }
        if (!packet.type || typeof packet.type !== 'string') {
            return;
        }
        if (packet.type === 'pong') {
            this.connectionHeartBeat = false;
            return;
        }
        this.state.onPacketReceive(packet);
    }

    /**
     * @returns {ClientInfo}
     */
    getDisplayData() {
        const result = new ClientInfo(this.id, this.username, null, this.status === ClientStatus.INGAME);
        if (this.character) {
            result.characterName = this.character.name;
        }
        return result;
    }

    /**
     * @param {Character} character
     */
    linkCharacter(character) {
        this.character = character;
        character.spawn(this);
        character.pilot = this;
        const packet = new PacketCharacterLink(character.getCharacterInfo());
        this.send(packet);
    }

    unlinkCharacter() {
        if (!this.character) {
            return;
        }
        this.character.despawn();
        this.character = null;
    }

    /**
     * @param {Packet} packet
     */
    send(packet) {
        if (!this.socket.destroyed) {
            this.socket.write(packet.complete());
        }
    }
}
