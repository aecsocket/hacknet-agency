import Packet from '../Packet.js';

export default class PacketTerminalMessage extends Packet {
    type = 'terminal-msg';

    message = '';

    /**
     * @param {string} message
     */
    constructor(message) {
        super();
        this.message = message;
    }
    
    /**
     * @param {PacketTerminalMessage} packet
     * @returns {*}
     */
    static serialize(packet) {
        return { message: packet.message };
    }
}
