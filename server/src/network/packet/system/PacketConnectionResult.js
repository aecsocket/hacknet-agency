import Packet from '../Packet.js';

/**
 * Result of the ongoing connection
 */
export default class PacketConnectionResult extends Packet {
    type = 'connection-result';

    static Result = {
        ACCEPTED: 0,
        ERROR: 1
    };

    result = PacketConnectionResult.Result.ERROR;
    error = '';

    constructor(type, err) {
        super();
        this.result = type;
        this.error = err;
    }

    /**
     * @param {PacketConnectionResult} packet
     * @returns {*}
     */
    static serialize(packet) {
        return {result: packet.result, error: packet.error};
    }
}
