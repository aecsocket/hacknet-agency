import Packet from '../Packet.js';

/**
 * Response
 *
 * Result of the authentication request
 */
export default class PacketAuthResult extends Packet {
    type = 'auth-result';

    static Result = {
        ACCEPTED: 0,
        ERROR: 1
    };

    result = PacketAuthResult.Result.ERROR;
    error = '';

    /**
     * @param {number} type
     * @param {string} err
     */
    constructor(type, err) {
        super();
        this.result = type;
        this.error = err;
    }

    /**
     * @param {PacketAuthResult} packet
     * @returns {*}
     */
    static serialize(packet) {
        return {result: packet.result, error: packet.error};
    }
}
