import Packet from '../Packet.js';

export default class PacketEnterWorld extends Packet {
    type = 'enter-world';

    constructor() {
        super();
    }
    
    /**
     * @param {PacketEnterWorld} packet
     * @returns {*}
     */
    static serialize(packet) {
        return { };
    }
}
