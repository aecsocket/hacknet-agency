import Packet from '../Packet.js';

export default class PacketCharacterLink extends Packet {
    type = 'character-link';

    characterInfo;

    constructor(characterInfo) {
        super();
        this.characterInfo = characterInfo;
    }
    
    /**
     * @param {PacketCharacterLink} packet
     * @returns {*}
     */
    static serialize(packet) {
        return { characterInfo: packet.characterInfo };
    }
}
