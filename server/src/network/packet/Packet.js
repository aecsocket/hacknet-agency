export default class Packet {
    type = 'none';

    constructor() {

    }

    /**
     * @param {Packet} packet
     * @returns {*}
     */
    static serialize(packet) {
        return {type: packet.type};
    }

    /**
     * @returns {*}
     */
    chainSerialize() {
        // @ts-ignore
        let preSerialized = this.constructor.serialize(this);
        let proto = this;
        do {
            proto = Object.getPrototypeOf(proto.constructor).prototype;
            // @ts-ignore
            preSerialized = Object.assign(preSerialized, proto.constructor.serialize(this));
        } while (proto.constructor.name !== 'Packet');
        return preSerialized;
    }

    /**
     * @virtual
     * @param {*} data
     * @returns {string}
     */
    build(data) {
        return JSON.stringify(data) + '\0';
    }

    complete() {
        return this.build(this.chainSerialize());
    }
}
