import State from './State.js';
import {ClientStatus} from '../../Client.js';

import GameState from './GameState.js';

/** @typedef {import('../../Client.js').default} Client */

/** @typedef {import('../../Character.js').default} Character */


export default class LobbyState extends State {
    isReady = false;
    
    listeners = {
        'lobby-info': (packet) => this.onInfoRequestPacket(packet),
        'lobby-select': (packet) => this.onCharacterSelectPacket(packet),
        'lobby-ready': (packet) => this.onPlayerReady(packet)
    };

    /**
     * @param {Client} client
     */
    constructor(client) {
        super(client);
    }

    enter() {
        this.client.status = ClientStatus.CONNECTED;
        this.client.server.onClientsUpdate();
    }

    exit() {

    }

    /**
     * @param {*} packet
     */
    onInfoRequestPacket(packet) {
        const worldPacket = this.client.server.getWorld().getWorldInfoPacket();
        this.client.send(worldPacket);
    }

    /**
     * @param {*} packet
     */
    onCharacterSelectPacket(packet) {
        if (typeof packet.character !== 'number') {
            return;
        }
        const character = this.client.server.getWorld().getCharacterFromId(packet.character);
        if (!character || character.pilot !== null) {
            return;
        }
        this.client.linkCharacter(character);
        this.client.server.onClientsUpdate();
    }

    /**
     * @param {*} packet
     */
    onPlayerReady(packet) {
        if (!this.client.character) {
            return;
        }
        this.client.setState(new GameState(this.client));
    }
}
