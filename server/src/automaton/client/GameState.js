import State from './State.js';

import {ClientStatus} from '../../Client.js';

import Command from './commands/Command.js';
import CommandConnect from './commands/CommandConnect.js';
import CommandDisconnect from './commands/CommandDisconnect.js';
import CommandChangeDir from './commands/CommandChangeDir.js';
import CommandList from './commands/CommandList.js';
import CommandTouch from './commands/CommandTouch.js';

import PacketEnterWorld from '../../network/packet/general/PacketEnterWorld.js';
import PacketTerminalMessage from '../../network/packet/game/PacketTerminalMessage.js';

import log, {LogType} from '../../Logger.js';

/** @typedef {import('../../Client.js').default} Client */

export default class GameState extends State {
    listeners = {
        'term-input': (packet) => this.onTermInputPacket(packet)
    };

    /**
     * @type {Object.<string, Class<Command>>}
     */
    commands = {
        'connect': CommandConnect,
        'disconnect': CommandDisconnect,
        'cd': CommandChangeDir,
        'ls': CommandList,
        'touch': CommandTouch
    };

    /**
     * @param {Client} client
     */
    constructor(client) {
        super(client);
    }

    enter() {
        this.client.status = ClientStatus.INGAME;

        const pack = new PacketEnterWorld();
        this.client.send(pack);
    }

    exit() {

    }

    /**
     * @param {*} packet
     */
    onTermInputPacket(packet) {
        if (!packet.content || typeof packet.content !== 'string') {
            return;
        }
        const args = Command.argumentParse(packet.content);
        if (this.commands[args[0]]) {
            try {
                this.commands[args[0]].execute(this.client.character, args);
            } catch (e) {
                log(LogType.WARNING, `Failed to run command ${args[0]}: ${e}`);
            }
        }
        else {
            const pack = new PacketTerminalMessage('Invalid command.');
            this.client.send(pack);
        }
    }
}
