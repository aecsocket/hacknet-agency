/** @typedef {import('../../Client.js').default} Client */

export default class State {
    /** @type {Client} */
    client;

    /**
     * @callback listener
     * @param {any} packet
     * @returns {void}
     */

    /** @type {Object.<string, listener>} */
    listeners = {};

    /**
     * @param {Client} client
     */
    constructor(client) {
        this.client = client;
    }

    enter() {

    }

    exit() {

    }

    /**
     * @param {*} object
     */
    onPacketReceive(object) {
        const listener = this.listeners[object.type];
        if (listener) {
            listener(object);
        }
    }
}
