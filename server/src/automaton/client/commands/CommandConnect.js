import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */

/**
 * @petition to rename this class ConnectCommand
 */
export default class CommandConnect extends Command {
    static get cmdName() {
        return 'connect';
    }

    static get help() {
        return 'connect [ADDRESS]\n\tConnect to an external computer via their address';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        if (args.length < 2) {
            character.writeToTerminal(CommandConnect.help);
            return;
        }

        let targetComputer = null;
        if (args[1] === 'localhost') {
            targetComputer = character.homeNode;
        }
        else {
            targetComputer = character.getWorld().getNodeFromAddress(args[1]);
        }

        if (!targetComputer) {
            character.writeToTerminal(`Failed to connect:\n\tCould not find computer at ${args[1]}`);
        }
        else {
            if (character.currentNode) {
                character.disconnect();
                character.writeToTerminal('Disconnected');
            }
            character.connect(targetComputer);
            character.writeToTerminal(`:: Connection Established ::\nConnected to ${targetComputer.displayName}@${targetComputer.address}`);
        }
    }
}
