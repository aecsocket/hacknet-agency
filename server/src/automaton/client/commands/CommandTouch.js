import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */

export default class CommandTouch extends Command {
    static get cmdName() {
        return 'touch';
    }

    static get help() {
        return 'touch [FILE]\n\tCreates a new file';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        if (args.length < 2) {
            character.writeToTerminal(CommandTouch.help);
            return;
        }
    }
}
