import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */

export default class CommandRemove extends Command {
    static get cmdName() {
        return 'rm';
    }

    static get help() {
        return 'rm [FILE]\n\tDeletes a file';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        if (args.length < 2) {
            character.writeToTerminal(CommandRemove.help);
            return;
        }
    }
}
