/** @typedef {import('../../../Character.js').default} Character */

export default class Command {
    /**
     * @abstract
     * @returns {string}
     */
    static get cmdName() {
        throw 'Unimplemented';
    }

    /**
     * @abstract
     * @returns {string}
     */
    static get help() {
        throw 'Unimplemented: There\'s no help for you.';
    }

    /**
     * @abstract
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        throw 'Unimplemented';
    }

    /**
     * @param {string} string
     * @returns {string[]}
     */
    static argumentParse(string) {
        const args = [];
        let cur = '';
        let escape = false;
        let group = false;
        let i = 0;
        while (i < string.length) {
            const c = string[i];
            switch (c) {
            case '\\':
                if (escape) {
                    escape = false;
                    cur += c;
                }
                else {
                    escape = true;
                }
                break;
            case '"':
                if (escape) {
                    escape = false;
                    cur += c;
                }
                else {
                    if (group) {
                        group = false;
                        args.push(cur);
                        cur = '';
                    }
                    else {
                        group = true;
                        if (cur !== '') {
                            return null;
                        }
                    }
                }
                break;
            case ' ':
                escape = false;
                if (group) {
                    cur += c;
                }
                else if (cur.length > 0) {
                    args.push(cur);
                    cur = '';
                }
                break;
            default:
                escape = false;
                cur += c;
                break;
            }
            i++;
        }
        if (group) {
            return null;
        }
        if (cur.length > 0) {
            args.push(cur);
        }
        return args;
    }
}
