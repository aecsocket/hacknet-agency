import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */
/** @typedef {import('../../../node/component/fs/File.js').default} File */
/** @typedef {import('../../../node/component/fs/Directory.js').default} Directory */

/**
 * @petition to rename this class ListCommand
 */
export default class CommandList extends Command {
    static get cmdName() {
        return 'ls';
    }

    static get help() {
        return 'ls (FILES)\n\tLists all files in the current directory or the specified directory';
    }

    /**
     * @param {Character} character
     * @param {File} file
     */
    static writeFileInfo(character, file) {
        if (file.type === 'dir') {
            for (const subFile of (/** @type {Directory} */ (file)).children) {
                character.writeToTerminal(`${subFile.name}`);
            }
        }
        else {
            character.writeToTerminal(`${file.name}`);
        }
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        let files = [character.currentDirectory];
        if (args.length >= 2) {
            files = files.getFiles(args[1]);
            if (!files) {
                character.writeToTerminal(`${args[0]}: no such file or directory: ${args[1]}`);
                return;
            }
        }
        for (const file of files) {
            CommandList.writeFileInfo(character, file);
        }
    }
}
