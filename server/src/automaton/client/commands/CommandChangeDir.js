import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */

export default class CommandChangeDir extends Command {
    static get cmdName() {
        return 'cd';
    }

    static get help() {
        return 'cd [DIRECTORY]\n\tChanges directory to another';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        if (args.length < 2) {
            character.writeToTerminal(CommandChangeDir.help);
            return;
        }

        const target = character.currentDirectory.getFile(args[2]);
        if (!target) {
            character.writeToTerminal(`${args[0]}: no such directory: ${args[2]}`);
            return;
        }

        character.changeDir(target);
    }
}
