import GlobalExts from './src/misc/GlobalExts.js';
GlobalExts();

import Server from './src/Server.js';

const server = new Server();
server.init();
